using IBAN;
using NUnit.Framework;

namespace IBANTests
{
    public class IbanControllerTest
    {
        [Test]
        // CounteryandPrüfziferControlling testing
        public void CounteryandPrüfziferControlling()
        {
            bool result = IBANController.CounteryandPrüfziferControlling("DE46232151211234567895");
            Assert.AreEqual(true, result);
            Assert.Pass();
        }
    }
}