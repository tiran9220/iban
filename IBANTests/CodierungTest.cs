using IBAN;
using NUnit.Framework;
namespace IBANTests
{
    public class CodierungTest
    {
        [Test]
        // Entcodingkonversions Test
        public void Entcodingkonversions()
        {
            string result = Codierung.Entcodingkonversions('d');
            Assert.AreEqual("13",result);
            Assert.Pass();
        }
        [Test]
        public void Decodingkonversions()
        {
            string result = Codierung.Decodingkonversions("13");
            Assert.AreEqual("D",result);
            Assert.Pass();
        }
        [Test]
        public void DecodingBban()
        {
            string result = Codierung.DecodingBban("02542dd35");
            Assert.AreEqual("02542131335",result);
            Assert.Pass();
        }
    }
}