using IBAN;
using NUnit.Framework;

namespace IBANTests
{
    public class IbanCheckTest
    {
        [Test]
        // CounteryandPrüfziferControlling testing
        public void PrüfzifferDefinition()
        {
            string result = IBANCheck.PrüfzifferDefinition("232151211234567895","DE");
            Assert.AreEqual("46", result);
            Assert.Pass();
        }
    }
}