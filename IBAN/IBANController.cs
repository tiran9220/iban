namespace IBAN
{
    public class IBANController:Menu
    {

        public static bool CounteryandPrüfziferControlling(string iban)
        {
            string counterycode = "";
            string prüfzifer = "";
            string bban = "";
            bool meldung = false;
            for (int i = 0; i < iban.Length; i++)
            {
                if (i < 2)
                {
                    counterycode +=iban[i];
                }

                if (i > 1 && i < 4)
                {
                    prüfzifer += iban[i];
                }

                if (i > 3 && i < iban.Length)
                {
                    bban += iban[i];
                }
            }

            if (IBANCheck.PrüfzifferDefinition(bban, counterycode) == prüfzifer)
            {
                meldung = true;
            }

            return meldung;
        }

    }
}