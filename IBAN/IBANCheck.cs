using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace IBAN
    {
        public class IBANCheck:Codierung
        {
            static string LandDecodieren(){
                string decodeland ="";
                for (int i = 0; i < Land.Length; i++)
                    {
                        decodeland += Entcodingkonversions(Land[i]);
                    }
                    return decodeland;
            }
            public static string PrüfzifferDefinition(string bban,string land)
            {
                string ibanstring = "";
                if (bban == "0")
                { 
                    ibanstring = DecodingBban(Blz+Kontonr) + LandDecodieren() + "00";
                }
                else
                {
                    string decodigland = "";
                    for (int i = 0; i < land.Length; i++)
                    {
                        decodigland += Entcodingkonversions(land.ToUpper()[i]);
                    }
                    ibanstring = DecodingBban(bban)+ decodigland + "00";
                }
                decimal iban = 0;
           
                iban = Convert.ToDecimal(ibanstring.Replace(" ", ""));
                Decimal modulo = 98 - (iban % 97);
                return modulo.ToString(CultureInfo.InvariantCulture);
            
            }

            public static string IbanDeffination()
            {
                String entcoding = DecodingBban(Blz + Kontonr) + LandDecodieren() + PrüfzifferDefinition("0", "0");
                String counterycodeandPrüfer = "";
                String bban = "";
                for (int i = entcoding.Length - 6; i < entcoding.Length; i++)
                {
                    counterycodeandPrüfer += entcoding[i];
                }

                string bbayhelpstring = "";
                for (int i = 0; i < entcoding.Length - 6; i++)
                {
                    bbayhelpstring += entcoding[i];
                }

                String bbaNectcode = EntcodingBban(bbayhelpstring);
                for (int i = 0; i < bbaNectcode.Length; i++)
                {
                    if (i % 4 != 0)
                    {
                        bban += bbaNectcode[i];

                    }
                    else
                    {
                        bban += " " + bbaNectcode[i];
                    }
                }

                string first = "";
                string secound = "";
                string prüfzifer = "";
                for (int j = 0; j < counterycodeandPrüfer.Length; j++)
                {
                    if (j < 2)
                    {
                        first += counterycodeandPrüfer[j];
                    }

                    if (j >= 2 && j < 4)
                    {
                        secound += counterycodeandPrüfer[j];
                    }

                    if (j >= 4)
                    {
                        prüfzifer += counterycodeandPrüfer[j];
                    }
                }

                while (!Regex.Match(Decodingkonversions(first), @"[A-Z]").Success||!Regex.Match(Decodingkonversions(secound), @"[A-Z]").Success)
                { 
                    Console.WriteLine("Es ist einen Fehler bei der automatischen Generierung gefunden.");
                    Console.WriteLine("Versuchen Sie es nochmal zu wiederholen!");
                    AddIban();
                    return null;
                }
                return Decodingkonversions(first) + Decodingkonversions(secound) + prüfzifer + bban;
            }
        }
    }