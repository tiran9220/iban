using System;
using System.Text.RegularExpressions;

namespace IBAN
{
    public class Codierung:Menu
    {
        public static string Entcodingkonversions(char zeichen){
            string str=Convert.ToString(zeichen);
            char buchstabe = Convert.ToChar(str.ToLower());
            int index = 10;
            for (char charArr = 'a'; charArr <= 'z'; charArr++)
            {
                if(buchstabe!=charArr){
                    index=index+1;
                }
                else{
                    break;
                }
            } 
            return index.ToString();
        }

        public static string Decodingkonversions(string item){
            int index = Convert.ToInt16(item)-10;
            int iteration = 0;
            string code=" ";
            for (char charArr = 'a'; charArr <= 'z'; charArr++)
            {
                if(iteration==index){
                    code = Convert.ToString(charArr);
                    iteration=0;
                    break;
                }
                iteration++;
            } 
            return code.ToUpper();
        }

        public static string DecodingBban(string bban){
            string bbannummer="";
            int index = 0;
            int control=0;
            for (int i = 0; i < bban.Length; i++)
            {
                if (Regex.Match(Convert.ToString(bban[i]).ToLower(), @"[a-z]").Success){
                    bbannummer +=  Entcodingkonversions(bban[i]);
                    Position[index++] = i+control;
                    control++;
                    Ziferinbban=true;
                }
                else
                {
                    bbannummer+=bban[i];
                }
                    
            }
            return bbannummer;
        }

        public static string EntcodingBban(string decodebban){
            int index = 0;
            string bban = "";
            if(Ziferinbban){
                for (int i = 0; i < decodebban.Length; i++){
                    if(Position.Length>=index&&Position[index]!=0){
                        if(i==Position[index]){
                            bban += Decodingkonversions(Convert.ToString(decodebban[i])+Convert.ToString(decodebban[++i]));
                            index++;
                        }
                        else
                        {
                            bban += decodebban[i];
                        }
                    }
                    else
                    {
                        bban += decodebban[i];
                    }
                }
            }
            else
            {
                bban = decodebban;
            }
            return bban;       
        }
    }
}
