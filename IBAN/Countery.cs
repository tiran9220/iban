using System;
using System.Text.RegularExpressions;

namespace IBAN
{
    public class Countery:Codierung
    {
        public static string ValidBlz(string land)
        {
            String blz = "";
            switch (land)
            {
                case "de": 
                     do
                     {
                          Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 8 Ziffern erlaubt): ");
                          blz = Console.ReadLine();
                     } while (blz != null && (blz.Length != 8 || !Regex.Match(blz, @"[0-9]").Success));
                        break;
                   case "be":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 3 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 3 || !Regex.Match(blz, @"[0-9]").Success));
                        break;
                   case "bg":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 8 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length < 8 || blz.Length > 8 || !Regex.Match(blz, @"[0-9]").Success));
                        break;
                   case "dk":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 4 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 4 || !Regex.Match(blz, @"[0-9]").Success));                        
                        break;
                   case "ee":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 2 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 2 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "fi":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 6 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 6 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "fr":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 10 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 10 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "gr":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 7 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 7 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "ie":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 10 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 10 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "is":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 6 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 6 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "it":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 10 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 10 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "iv":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 8 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 8 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "li":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 5 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 5 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "lt":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 5 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 5 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "lu":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 3 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 3 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "mt":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 9 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 9 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "mc":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 10 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 10 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "nl":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 4 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 4 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "no":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 4 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 4 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                    case "at":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 5 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 5 || !Regex.Match(blz, @"[0-9]").Success));                         
                         break;
                   case "pl":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 7 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 7 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "pt":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 8 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 8 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "ro":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 4 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 4 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "se":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 3 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 3 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "ch":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 5 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 5 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "sk":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 10 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 10 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "si":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 5 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 5 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "es":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 8 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 8 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "cz":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 4 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 4 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "hu":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 7 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 7 || !Regex.Match(blz, @"[0-9]").Success));                       
                        break;
                   case "gb":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 10 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 10 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                   case "cy":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun Instituts-Identifikation ein (nur 8 Ziffern erlaubt): ");
                             blz = Console.ReadLine();
                        } while (blz != null && (blz.Length != 8 || !Regex.Match(blz, @"[0-9]").Success));                      
                        break;
                    default:
                        break;
            }
            return blz;
        }
        public static string ValidNr(String land)
        {
            String kontonr = "";
            switch (land)
            {
                case "de": 
                     do
                     {
                          Console.Write("\nGeben Sie bitte nun die 10 zahlige Bankkonto-Nummer ein : ");
                          kontonr = Console.ReadLine();
                     } while (kontonr != null && (kontonr.Length != 10 || !Regex.Match(kontonr, @"[0-9]").Success));
                     
                     break;
                   case "be":
                        
                        break;
                   case "bg": 
                        
                        break;
                   case "dk":
                       
                        break;
                   case "ee":
                       
                        break;
                   case "fi":
                       
                        break;
                   case "fr":
                       
                        break;
                   case "gr":
                       
                        break;
                   case "ie":
                       
                        break;
                   case "is":
                       
                        break;
                   case "it":
                      
                        break;
                   case "iv":
                       
                        break;
                   case "li":
                        
                        break;
                   case "lt":
                        
                        break;
                   case "lu":
                        
                        break;
                   case "mt":
                       
                        break;
                   case "mc":
                       
                        break;
                   case "nl":
                       
                        break;
                   case "no":
                        
                        break;
                    case "at":
                         do
                         {
                              Console.Write("\nGeben Sie bitte nun die 11 zahlige Bankkonto-Nummer ein : ");
                              kontonr = Console.ReadLine();
                         } while (kontonr != null && (kontonr.Length != 11 || !Regex.Match(kontonr, @"[0-9]").Success));
                         break;
                   case "pl":
                       
                        break;
                   case "pt":
                        
                        break;
                   case "ro":
                       
                        break;
                   case "se":
                        
                        break;
                   case "ch":
                        do
                        {
                             Console.Write("\nGeben Sie bitte nun die 12 zahlige Bankkonto-Nummer ein (eine Buchtabe erlaub): ");
                             kontonr = Console.ReadLine();
                        } while (kontonr != null && (kontonr.Length != 12 || !Regex.Match(kontonr, @"[0-9a-zA-Z]").Success));
                        
                        break;
                   case "sk":
                       
                        break;
                   case "si":
                       
                        break;
                   case "es":
                        
                        break;
                   case "cz":
                        
                        break;
                   case "hu":
                       
                        break;
                   case "gb":
                        
                        break;
                   case "cy":
                        
                        break;
            }
            return kontonr;
        }
    }
}
