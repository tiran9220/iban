using System;
using System.IO;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using static IBAN.Countery;

namespace IBAN
{
    public class Menu
    {
        protected static string Land;
        protected static  string Blz;
        protected static string Kontonr;
        protected static string Iban;

        protected static bool Ziferinbban = false;
        protected static int [] Position = new int[10];
        protected static int Countibans = 0;
        protected static string[][] ibanarraylist = new String[100][];
        public void ShowHeader(){
            Console.WriteLine("*****************************************************");
            Console.WriteLine("*              ____________________________         *");
            Console.WriteLine("*             |                            |        *");
            Console.WriteLine("*             |  Willkommen zu Iban Check  |        *");
            Console.WriteLine("*             |____________________________|        *");
            Console.WriteLine("*                                                   *");
            Console.WriteLine("*- Program wurde von Grigoryan und Faber entwickelt-*");
            Console.WriteLine("*                                                   *");
            Console.WriteLine("* -- Lassen Sie uns Ihre IBAN sicher generieren --  *");
            Console.WriteLine("*                                                   *");
            Console.WriteLine("*****************************************************\n");
            Console.WriteLine("Los gehts\n");
            ShowHelpMenu();
        }
        static void ShowHelpMenu(){
            Console.Write("Geben Sie bitte nun ein Befehl oder ? ein: ");
            string eingabe = Console.ReadLine();
            switch(eingabe){
                case "?":
                    ShowHelp();
                    break;
                case "+":
                    AddIban();
                    ShowHelpMenu();
                    break;
                case "n":
                    AddIban();
                    ShowHelpMenu();
                    break;
                case "p":
                    CheckIban();
                    ShowHelpMenu();
                    break;
                case "a":
                    IbaNarray(ibanarraylist);
                   ShowHelpMenu();
                    break;
                case "e":
                    ExportIban(ibanarraylist,"IbanList");
                    ShowHelpMenu();
                    break;
                case "i":
                    ImportCsvIban("IbanList");
                    ShowHelpMenu();
                    break;
                default:
                    ShowHelpMenu();
                    break;

            }
        }
        static void ShowHelp(){
            Console.WriteLine("\n+: IBAN erzeugen oder automatisch generieren lassen");
            Console.WriteLine("p: IBAN prüfen");
            Console.WriteLine("a: Die erzeugten IBAN Liste anzeigen");
            Console.WriteLine("i: IBAN Liste von ibanlist.cvb importieren und alle prüfen lasen");
            Console.WriteLine("e: IBAN Liste zum ibanlist.cvb exportieren\n");

            ShowHelpMenu();
        }
       public static void AddIban(){
            Console.Write("\nGeben Sie bitte nun die Ländercode in dem folgenden Format(z.B de) ein! ");
            Land = Console.ReadLine()?.Replace(" ","");
            while (Land != null && !Regex.Match(Land.ToLower(), @"[a-z]").Success) {
                AddIban();
            }
            if (Land != null)
                switch (Land.ToLower())
                {
                    case "de":
                        Console.WriteLine("\nSie haben die ISO- Code von Deutschland eingetragen.");
                        BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "be":
                       Console.WriteLine("\nSie haben die ISO- Code von Belgien eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "bg":
                       Console.WriteLine("\nSie haben die ISO- Code von Bulgarien eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "dk":
                       Console.WriteLine("\nSie haben die ISO- Code von Dänemark eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "ee":
                       Console.WriteLine("\nSie haben die ISO- Code von Estland eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "fi":
                       Console.WriteLine("\nSie haben die ISO- Code von Finnland eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "fr":
                       Console.WriteLine("\nSie haben die ISO- Code von Frankreich eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "gr":
                       Console.WriteLine("\nSie haben die ISO- Code von Griechenland eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "ie":
                       Console.WriteLine("\nSie haben die ISO- Code von Irland eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "is":
                       Console.WriteLine("\nSie haben die ISO- Code von Island eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "it":
                       Console.WriteLine("\nSie haben die ISO- Code von Italien eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "iv":
                       Console.WriteLine("\nSie haben die ISO- Code von Lettland eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "li":
                       Console.WriteLine("\nSie haben die ISO- Code von Liechtenstein eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "lt":
                       Console.WriteLine("\nSie haben die ISO- Code von Litauen eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "lu":
                       Console.WriteLine("\nSie haben die ISO- Code von Luxenburg eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "mt":
                       Console.WriteLine("\nSie haben die ISO- Code von Malta eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "mc":
                       Console.WriteLine("\nSie haben die ISO- Code von Monaco eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "nl":
                       Console.WriteLine("\nSie haben die ISO- Code von Niederlande eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "no":
                       Console.WriteLine("\nSie haben die ISO- Code von Norwegen eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                    case "at":
                        Console.WriteLine("\nSie haben die ISO- Code von Österreich eingetragen.");
                        BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "pl":
                       Console.WriteLine("\nSie haben die ISO- Code von Polen eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "pt":
                       Console.WriteLine("\nSie haben die ISO- Code von Portugal eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "ro":
                       Console.WriteLine("\nSie haben die ISO- Code von Rumänien eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "se":
                       Console.WriteLine("\nSie haben die ISO- Code von Schweden eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "ch":
                       Console.WriteLine("\nSie haben die ISO- Code von Schwiez eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "sk":
                       Console.WriteLine("\nSie haben die ISO- Code von Slowakei eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "si":
                       Console.WriteLine("\nSie haben die ISO- Code von Slowenien eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "es":
                       Console.WriteLine("\nSie haben die ISO- Code von Spanien eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "cz":
                       Console.WriteLine("\nSie haben die ISO- Code von Tschechische Republik eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "hu":
                       Console.WriteLine("\nSie haben die ISO- Code von Ungarn eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "gb":
                       Console.WriteLine("\nSie haben die ISO- Code von Vereinigtes Königreich eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                   case "cy":
                       Console.WriteLine("\nSie haben die ISO- Code von Zypern eingetragen.");
                       BlzundKnummerGenerator(8, 10,"0123456789");
                        break;
                    default:
                        Console.WriteLine("\nSie haben falsche ISO- Code eingetragen.\n" +
                                          "Bitte geben Sie korrekte Landescode ein\n" +
                                          "Land_______________________________ISO Code(Landescode)\n" +
                                          "Deutschland________________________de\n" +
                                          "Belgien____________________________be\n" +
                                          "Bulgarien__________________________bg\n" +
                                          "Dänemark___________________________dk\n" +
                                          "Estland____________________________ee\n" +
                                          "Finnland___________________________fi\n" +
                                          "Frankreich_________________________fr\n" +
                                          "Griechenland_______________________gr\n" +
                                          "Irland_____________________________ie\n" +
                                          "Island_____________________________is\n" +
                                          "Italien____________________________it\n" +
                                          "Lettland___________________________iv\n" +
                                          "Liechtenstein______________________li\n" +
                                          "Litauen____________________________lt\n" +
                                          "Luxenburg__________________________lu\n" +
                                          "Malta______________________________mt\n" +
                                          "Monaco_____________________________mc\n" +
                                          "Niederlande________________________nl\n" +
                                          "Norwegen___________________________no\n" +
                                          "Österreich_________________________at\n" +
                                          "Polen______________________________pl\n" +
                                          "Portugal___________________________pt\n" +
                                          "Rumänien___________________________ro\n" +
                                          "Schweden___________________________se\n" +
                                          "Schweiz____________________________ch\n" +
                                          "Slowakei___________________________sk\n" +
                                          "Slowenien__________________________si\n" +
                                          "Spanien____________________________es\n" +
                                          "Tschechische Republik______________cz\n" +
                                          "Ungarn_____________________________hu\n" +
                                          "Vereinigtes Königreich_____________gb\n" +
                                          "Zypern_____________________________cy");
                        AddIban();
                        break; 
                }
            if(IBANCheck.IbanDeffination()!=null)
            ibanarraylist[Countibans++] = new String[] {"IBAN ",IBANCheck.IbanDeffination() };
            ShowHelp();
       }

        private static void BlzundKnummerGenerator(int Blzlength, int Knummerlength, String characters)
        {
            Console.Write("\nMöchten Sie automatisch generierte Blz haben?(j/n):");
            switch (Console.ReadLine())
            {
                case "j":
                    Blz = RandomNumber(Blzlength, characters);
                                
                    break;
                case "n":
                    Blz = ValidBlz(Land);
                    break;
                default:
                    Console.WriteLine("Sie haben weder j noch n gedrückt versuchen Sie es nochmal");
                    AddIban();
                    break;
            }
            Console.Write("\nMöchten Sie automatisch generierte Kontonummer haben?(j/n):");
            switch (Console.ReadLine())
            {
                case "j":
                    Kontonr = RandomNumber(Knummerlength, characters);
                    break;
                case "n":
                    Kontonr = ValidNr(Land);
                    break;
                default:
                    Console.WriteLine("Sie haben weder j noch n gedrückt versuchen Sie es nochmal");
                    AddIban();
                    break;
            }
        }

        static void CheckIban(){
            Console.Write("\nGeben Sie bitte nun volständige 22-zählige IBAN in dem folgenden Format(DE01 0011 1222 1544 1211 1200 12) ein! ");
            Iban= Console.ReadLine();
            if (Iban != null && IBANController.CounteryandPrüfziferControlling(Iban.Replace(" ", "")))
            {
                Console.WriteLine("Die engegebene IBAN ist korrekt.");
                Console.ReadKey();
            }
            else
            { 
                Console.WriteLine("Die engegebene IBAN ist nicht richtig.\nVersuchen Sie es noch einmal einzugeben!");
                ShowHelp();
            }
        }

        static void IbaNarray(String [][] ibanArray)
        {
            for (int i = 0; i < Countibans; i++)
            {
                for (int j = 0; j < ibanArray[i].Length; j++)
                {
                    Console.Write(ibanArray[i][j]);
                }
                Console.WriteLine();
            }
        }

        static void ExportIban(String [][] exportArray,String dateiName)
        {
            Console.WriteLine(GetDataFile(dateiName));
            using (StreamWriter sw = new StreamWriter(GetDataFile(dateiName), true, new UTF8Encoding(true)))
            {
                using (StreamReader streamReader=new StreamReader(GetDataFile(dateiName),Encoding.Default, true))
                {
                    if (streamReader.ReadLine() != "IBAN_List")
                    {
                        sw.WriteLine("IBAN_List" );
                    }
                }
                if (Countibans > 0)
                {
                    for (int i = 0; i < Countibans; i++)
                    {
                        for (int j = 0; j < exportArray[i].Length; j++)
                        {
                            sw.Write(exportArray[i][j]);
                        }
                        sw.WriteLine();
                    }
                    Console.WriteLine(Countibans + " Iban Liste wurde gespeichert!");
                    Console.WriteLine("\nAuf Wiedersehen.");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Es wurde keine Datensätze gefunden!!!\n");
                    ShowHelp();
                }
            }
        }

        private static void ImportCsvIban(String dateiName)
        {
            try
            {
                String [][] newimport=new string[100][];
                int index = 0;
                using (StreamReader streamReader = new StreamReader(GetDataFile(dateiName), Encoding.Default, true))
                {
                    String iban="";
                    while ((iban = streamReader.ReadLine()) != null)
                    {
                        if(iban!="IBAN_List")
                        {
                            ibanarraylist[Countibans++] = new[] {iban};
                            newimport[index++] = iban.Split("IBAN ");
                        }
                    }
                }
                Console.Write("Alle importierte Ibans prüfen lassen j/n: ");
                switch (Console.ReadLine())
                {
                    case "j" :
                        for (int i = 0; i < index; i++)
                        {
                            for (int j = 0; j < newimport[i].Length; j++)
                            {
                                if(IBANController.CounteryandPrüfziferControlling(newimport[i][j].Replace(" ","")))
                                {
                                    Console.Write(newimport[i][j]+" Checked");
                                }
                                Console.WriteLine();
                            }
                        }
                       
                       // IbaNarray(newimport);
                        break;
                    case  "n" :
                        ShowHelpMenu();
                        break;
                    default:
                        Console.WriteLine("Sie haben falsches Eingegeben.");
                        Console.WriteLine("Versuchen Sie es nochmal");
                        ShowHelpMenu();
                        break;
                }
                ShowHelpMenu();
            }
            catch (Exception e) 
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }


        private static string GetDataFile(String dateiName)
        {
            // Wir die Datei in den folgenden Ordner gespeichert.
            //IBAN/bin/Debug/netcoreapp<Netcoer Version>/ibanlist.csv
            string currentDirectory = Environment.CurrentDirectory;
            return Path.Combine(currentDirectory, dateiName+".csv");
        }
        public static string RandomNumber(int length, String characters){
            Random random = new Random();
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++) {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
    }
}
